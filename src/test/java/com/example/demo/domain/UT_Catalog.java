/*
 * Algebra labs.
 */

package com.example.demo.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.example.demo.config.SpringConfig;
import com.example.demo.service.Catalog;

public class UT_Catalog {
	@Test
	public void catalogTest() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

		Catalog catalog = ctx.getBean(Catalog.class);
		Catalog catalog2 = ctx.getBean(Catalog.class);
		
		assertTrue("catalogs should not be equal", catalog != catalog2);

		ctx.close();
	}

}
