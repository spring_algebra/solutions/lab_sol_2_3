/*
 * Algebra labs.
 */
 
package com.example.demo.config;

import javax.inject.Inject;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.example.demo.persistence.ItemRepository;
import com.example.demo.service.Catalog;
import com.example.demo.service.CatalogImpl;

// Declare as a configuration class
@Configuration
public class SpringServicesConfig {
	
	// Inject the repository
	@Inject
	ItemRepository repository;

	// Declare the catalog bean definition
	@Bean
	@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public Catalog catalog() {
		CatalogImpl catalog = new CatalogImpl();
		catalog.setItemRepository(	repository	);
		return catalog;
	}

}